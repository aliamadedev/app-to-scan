package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class DemoApplication {


	@GetMapping("/")
	public String home() {
		return "Spring is here!";
	}

    @GetMapping("/loggedsd")
	public String login(
		@RequestParam(value = "username") String username,@RequestParam(value = "password") String password) {
		// Create unsafe query by concatenating user defined data with query string
		String query = "SELECT secret FROM Users WHERE (username = '" + username + "' AND NOT role = 'admin')";
		// ... OR ...
		// Insecurely format the query string using user defined data 
		String qusr = String.format("SELECT secret FROM Users WHERE (username = '%s' AND NOT role = 'admin')", username);
		return query;
	}
	
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
